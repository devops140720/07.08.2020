# 1
# write function mySqr with parameter x
# and return x ** 2
# 2
# write a function myMul with parameters x, y
# and returns x + y
# 3
# write a function myFactorial with parameter x
# this function calculate the x! factorial (1*2*3*4...*x)
# and return this factorial
# 4
# write a function myCheckEven with parameter x
# return boolean True if x is even (2, 4, 10, ...)
# return False if it is odd  (1, 3, 5 ...)
# 5
# write a function myAverage with parameter l1 (=list)
# return the average of this list
# 6
# write a function myConcat with parameters s1, s2 (=string)
# return s1 + " " + s2